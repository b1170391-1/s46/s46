import {Container, Row, Col, Card} from "react-bootstrap"

export default function Highlights() {
	return (
		
		<Container fluid className="mb-4 mb-2">
			<Row className="mt-4">
				
				<Col xs={12} md={4}>
					<Card className="cardHighlights p-3">
					  <Card.Body>
					    <Card.Title>Learn From Home</Card.Title>
					    <Card.Text>
					      Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas accusamus similique voluptate, voluptatum incidunt et tenetur rem quos est error aliquid molestias maxime nisi ipsam consequuntur modi expedita tempora recusandae?
					    </Card.Text>
					  </Card.Body>
					</Card>
				</Col>

				

				<Col xs={12} md={4}>
					<Card className="cardHighlights p-3">
					  <Card.Body>
					    <Card.Title>Study Now Pay Later</Card.Title>
					    <Card.Text>
					      Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas accusamus similique voluptate, voluptatum incidunt et tenetur rem quos est error aliquid molestias maxime nisi ipsam consequuntur modi expedita tempora recusandae?
					    </Card.Text>
					  </Card.Body>
					</Card>
				</Col>
				

				<Col xs={12} md={4}>
					<Card className="cardHighlights p-3">
					  <Card.Body>
					    <Card.Title>Be Part of Our Community</Card.Title>
					    <Card.Text>
					      Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas accusamus similique voluptate, voluptatum incidunt et tenetur rem quos est error aliquid molestias maxime nisi ipsam consequuntur modi expedita tempora recusandae?
					    </Card.Text>
					  </Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)
}